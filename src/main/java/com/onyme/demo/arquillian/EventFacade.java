/**
 * 
 */
package com.onyme.demo.arquillian;

import javax.ejb.Stateless;

/**
 * @author tvibes
 *
 */
@Stateless
public class EventFacade extends AbstractJPASessionFacadeImpl<Event> {

}
