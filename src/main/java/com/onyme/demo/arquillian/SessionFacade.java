package com.onyme.demo.arquillian;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public interface SessionFacade<T, Id extends Serializable> {

	/**
	 * <p>
	 * 	Get the entity with the specified type and id from the datastore.
	 * <p>
	 * If none is found, return {@code null}.
	 */	
	T read(Id id);
	
	
	/**
	 * <p>
	 * 	Get a list of all the objects of the specified type.
	 * </p>
	 * @return {@code List<T>}
	 */
	List<T> list();
	
		
	/**
	 * <p>
	 * 	Sauvegarde ou met à jour une entité de type T
	 * </p>
	 * @param entity l'entité(l'objet) à créer.	 
	 */
	void createOrUpdate(T entity);
	
	/**
	 * Supprimé l'entité de la base.
	 * @param entity l'entité à supprimer.
	 */
	void delete(T entity);
	
}