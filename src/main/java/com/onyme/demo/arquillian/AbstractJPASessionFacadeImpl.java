package com.onyme.demo.arquillian;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 * JPA based implementation of our persitence service
 * @author tvibes
 *
 * @param <T>  type of Entity the facade can handle
 */
public abstract class AbstractJPASessionFacadeImpl<T> implements SessionFacade<T, Serializable> {
	
	@PersistenceContext
	private EntityManager em;
	
	private Class<T> persistentClass;
	
	public AbstractJPASessionFacadeImpl() {		
		this.persistentClass = tryGetPersitedClass();
	}
	
	@SuppressWarnings("unchecked")
	private Class<T> tryGetPersitedClass() {	
		Class<?> clazz = getClass();		
		Type type = clazz.getGenericSuperclass();
		while ( !(type instanceof ParameterizedType) ) {
			clazz = clazz.getSuperclass();
			type = clazz.getGenericSuperclass();
		}		
		return (Class<T>) ((ParameterizedType) type).getActualTypeArguments()[0];		
	}
	
	@Override		
	public List<T> list() {
		CriteriaBuilder crit;	
		crit = em.getCriteriaBuilder();
		CriteriaQuery<T> cq = crit.createQuery(persistentClass);
		Root<T> root = cq.from(persistentClass);
		
		cq.select(root);
		
		TypedQuery<T> tq = em.createQuery(cq);
		return tq.getResultList();
	}

	@Override	
	public T read(Serializable id) {
		return em.find(persistentClass, id);
	}
	
	@Override
	public void createOrUpdate(T entity) {
		em.persist(entity);
	}	
	
	@Override
	public void delete(T entity) {
		em.remove(entity);
	}
}
