/**
 * 
 */
package com.onyme.demo.arquillian;

import static org.junit.Assert.*;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author tvibes
 *
 */
@RunWith(Arquillian.class)
public class MyFirstArquillianTest {
	
	@Deployment
	public static JavaArchive deployments(){
		return ShrinkWrap.create(JavaArchive.class);
	}

	@Test
	public void test() {

	}

}
