/**
 * 
 */
package com.onyme.demo.arquillian;

import static org.fest.assertions.Assertions.assertThat;

import javax.ejb.EJB;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.persistence.Cleanup;
import org.jboss.arquillian.persistence.CleanupStrategy;
import org.jboss.arquillian.persistence.DataSeedStrategy;
import org.jboss.arquillian.persistence.SeedDataUsing;
import org.jboss.arquillian.persistence.ShouldMatchDataSet;
import org.jboss.arquillian.persistence.TestExecutionPhase;
import org.jboss.arquillian.persistence.UsingDataSet;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Integration Test of "events" persistence
 * @author tvibes
 *
 */
@RunWith(Arquillian.class)
@SeedDataUsing(DataSeedStrategy.CLEAN_INSERT)
@UsingDataSet("events.xml")
@Cleanup(phase=TestExecutionPhase.AFTER, strategy=CleanupStrategy.USED_TABLES_ONLY)
public class EventCRUDIT {

	@Deployment
	public static WebArchive deployments(){
		return ShrinkWrap.create(WebArchive.class, "events.war")
					.addClass(Event.class)
					.addClass(EventFacade.class) //don't need to add interface and super-class !? => glassfish-embedded only
					.addAsResource("META-INF/persistence.xml", "META-INF/persistence.xml")
					.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}
	
	@EJB 
	private EventFacade eventFacade;
	
	@Test
	public void should_create_Event(){
		//Given
		Event event = new Event("My first event!");
		
		//When
		eventFacade.createOrUpdate(event);
	}
	
	@Test
	public void should_read_Event(){
		//Given
		int eventId = 101;
		
		//When
		Event event = eventFacade.read(eventId);
		
		//Then
		assertThat(event).isNotNull();
		assertThat(event.getPayload()).isEqualTo("Process 100 verbatims");
	}
	
	@Test
	@ShouldMatchDataSet("testUpdate_events.xml")
	public void should_update_Event(){
		//Given
		int eventId = 101;
		Event event = eventFacade.read(eventId);
		event.setPayload("Updated event");
		
		//When
		eventFacade.createOrUpdate(event);
	}
	
	@Test
	@ShouldMatchDataSet("testDelete_events.xml")
	public void should_delete_Event(){
		//Given
		int eventId = 101;
		Event event = eventFacade.read(eventId);
		
		//When
		eventFacade.delete(event);
	}
	
	@Test
	public void should_list_events(){		
		assertThat(eventFacade.list()).hasSize(2);
	}
	
	
}
