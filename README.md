Arquillian Introduction
=======================

This project is a "step-by-setp" introduction to arquillian capabilities.
Each step is tagged.

Summary
-------

* Step 0 - Setup a maven-based project
* Step 1 - Add arquillian to our project, a glassfish container and a first integration test
* Step 2 - add an entity, setup persistence.xml and create a integration test for CRUD operations
* Step 3 - Add arquillian persistence, refactor the integration test
* Step 4 - Move the persistence code in an @EJB and refactore the integration test


